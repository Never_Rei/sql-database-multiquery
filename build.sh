#!/bin/bash

set -e -o pipefail

source .venv/bin/activate

cd databasemultiquery

flake8 databasemultiquery

python setup.py test
python setup.py build

