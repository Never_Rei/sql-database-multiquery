from setuptools import setup, find_packages

setup(
    name='Sql-Database-Multiquery',
    version='0.1',
    author='James Washington',
    author_email='jwashington104.jw@gmail.com',
    packages=find_packages(include=['databasemultiquery', 'databasemultiquery.*']),
    url='https://gitlab.com/Never_Rei/sql-database-multiquery',
    description='A tool for querying and concatenating query results from multiple databases into one return.',
    install_requires=[
        "click==8.0.3",
        "flake8==4.0.1",
        "fqdn==1.5.1",
        "greenlet==1.1.2",
        "mccabe==0.6.1",
        "numpy==1.22.3",
        "pandas==1.4.2",
        "psycopg2-binary==2.9.3",
        "pycodestyle==2.8.0",
        "pyflakes==2.4.0",
        "python-dateutil==2.8.2",
        "pytz==2022.1",
        "PyYAML==6.0",
        "six==1.16.0",
        "SQLAlchemy==1.4.29"          
    ]
)