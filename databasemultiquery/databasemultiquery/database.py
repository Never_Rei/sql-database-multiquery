import logging
from os import path
from abc import ABC, abstractmethod
from dataclasses import dataclass
from urllib.parse import quote_plus
from pandas import DataFrame


import yaml
import sqlalchemy

from databasemultiquery import settings, sql


class DangerousSQLException(Exception):
    """
    This exception is raised when a dangerous SQL statement is detected.
    """
    pass


@dataclass
class DatabaseConfig:
    """
    Database connection configuration parameters.

    Attributes:
    -----------
    user: str
        The username required to connect to the database.
    password: str
        The password required to connect to the database.
    host: str
        The hostname or IP address of the database server.
    port: int
        The port number of the database server.
    database: str
        The name of the database to connect to.
    driver: str
        The name of the database driver to use.

    Methods:
    --------
    load_config_from_object(config: dict)
        Loads the database connection configuration from a dictionary object.
    """
    user: str = None
    password: str = None
    host: str = None
    port: int = 0
    database: str = None
    driver: str = None
    read_only: bool = True

    __SETTERS = [
        'user',
        'password',
        'host',
        'port',
        'database',
        'driver',
        'read_only'
    ]

    def __init__(self, config_obj: dict = None):
        self.logger = logging.getLogger(__name__)
        if config_obj is not None:
            self.load_config_from_object(config_obj)

    def load_config_from_object(self, object: dict) -> None:
        for key in object:
            if key in self.__SETTERS:
                setattr(self, key, object[key])
            else:
                raise NameError(f"{key} is not a valid parameter.")

    @property
    def connection_string(self):
        for key in self.__SETTERS:
            if getattr(self, key) is None:
                raise AttributeError(f"{key} is not set.")
        return (f"{self.driver}://{self.user}:{quote_plus(self.password)}"
                f"@{self.host}:{self.port}/{self.database}")


class DatabaseConfigLoader:
    """
    This class creates a list of database configuration objects.

    Attributes:
    -----------
    config_objects: list
        A list of configuration objects.
    """
    __CONFIG_OBJECTS: list = []
    __LOADED_CONFIG_FILE: dict

    def __init__(self, config_file: str = None):
        self.logger = logging.getLogger(__name__)
        self.__load_config_file(config_file)
        self.__build_config_objects()

    def __load_config_file(self, config_file: str):
        """
        Loads a yaml config file.
        """
        if config_file is None:
            config_file = settings.DATABASE_CONFIG_FILE

        if path.exists(config_file):
            with open(config_file, 'r') as file:
                self.__LOADED_CONFIG_FILE = yaml.safe_load(file)
        else:
            raise FileNotFoundError(f"{config_file} does not exist.")

    def __build_config_objects(self):
        """
        Builds the database connection objects
        """
        for obj in self.__LOADED_CONFIG_FILE['database']:
            self.__CONFIG_OBJECTS.append(DatabaseConfig(obj))

    @property
    def config_objects(self) -> list:
        return self.__CONFIG_OBJECTS


class DatabaseClient(ABC):
    """
    This class is the base class for all database clients.

    It sets out the basic methods that all database clients must implement.
    """

    def __init__(self):
        self.logger = logging.getLevelName(__name__)

    @abstractmethod
    def connect(self, config: DatabaseConfig) -> None:
        raise NotImplementedError

    @abstractmethod
    def close(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def execute(self, query: sql.Sql, override_safety=False) -> None:
        raise NotImplementedError

    @abstractmethod
    def is_closed(self) -> bool:
        raise NotImplementedError


class PostgreSQLClient(DatabaseClient):
    def __init__(self, config: DatabaseConfig):
        self.logger = logging.getLogger(__name__)
        self.config = config

    def connect(self, config: DatabaseConfig) -> None:

        self.logger.info(f"Connecting to {config.database}@"
                         f"{config.host}:{config.port}")
        self.engine = sqlalchemy.create_engine(config.connection_string)
        self.conn = self.engine.connect().execution_options(
            postgresql_readonly=True
            )
        self.logger.info("Connection established, "
                         f"connection name: {self.conn}")

    def close(self) -> None:
        self.logger.info(f"Closing connection to {self.conn}")
        self.conn.close()

    def execute(self, query: sql.Sql, override_safety=False) -> None:
        if query.is_dangerous is True and override_safety is False:
            raise DangerousSQLException("Unsafe SQL detected.")

        result = None
        try:
            self.connect(self.config)

            if query.is_dangerous:
                self.logger.warning("Unsafe SQL detected and "
                                    "safety override is set.")
            self.logger.info("Executing SQL query.")
            result = self._output_formatter(self.conn.execute(query.sql))
            self.logger.info("Query executed successfully.")

        except Exception as e:
            self.logger.error(f"Error executing SQL query: {e}")
            raise e

        finally:
            self.conn.close()
            return result

    def _output_formatter(self, result: any) -> DataFrame:
        columns = result.keys()
        df = DataFrame(result.fetchall())
        if len(df) > 0:
            df.columns = columns
        else:
            df = DataFrame(columns=columns)

        return df

    def _render_csv(self, result: DataFrame) -> None:
        raise NotImplementedError

    def _render_xslx(self, result: DataFrame) -> None:
        raise NotImplementedError

    def is_closed(self) -> bool:
        return self.conn.closed


class MySQLClient(DatabaseClient):
    pass


class DatabaseClientFactory:
    """
    This class creates a database client object based on the database type.

    Attributes:
    -----------
    database_type: str
        The database type to create a client for.
    """
    __DATABASE_TYPES = {
        'postgresql': PostgreSQLClient,
        'mysql': MySQLClient
    }

    def __init__(self, database_type: str):
        self.logger = logging.getLogger(__name__)
        self.database_type = database_type

    def create_client(self, config: DatabaseConfig) -> DatabaseClient:
        if self.database_type in self.__DATABASE_TYPES:
            return self.__DATABASE_TYPES[self.database_type](config)
        else:
            raise ValueError(f"{self.database_type} is not a "
                             "valid database type.")


class DatabaseClientManager:
    """
    This class manages the database clients.

    Attributes:
    -----------
    config_file: str
        The path to the database configuration file.
        Defaults to /etc/databasemultiquery/database_config.yaml.
    """
    __CLIENTS: list = []

    def __init__(self, config_file: str = None):
        self.logger = logging.getLogger(__name__)
        self.__config_file = config_file
        self.database_config = DatabaseConfigLoader(self.__config_file)

        self.create_clients()

    def create_clients(self):
        for config in self.database_config.config_objects:
            self.__CLIENTS.append(
                DatabaseClientFactory(config.driver).create_client(config))

    def list_clients(self):
        for client in self.__CLIENTS:
            print(client.config)

    @property
    def get(self):
        return self.__CLIENTS

    def close_all(self) -> None:
        for client in self.__CLIENTS.values():
            client.close()
