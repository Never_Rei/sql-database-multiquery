import logging

UNSAFE_SQL_STATEMENT_LIST = [
    'DELETE',
    'DROP',
    'INSERT',
    'UPDATE',
    'CREATE',
]


class Sql:
    """
    This class represents an SQL statement.

    Attributes:
    -----------
    is_dangerous: bool
        Is the SQL statement dangerous?
    is_empty: bool
        Is this object empty?
    sql: str
        The SQL statement.
    """
    is_dangerous: bool = False
    is_empty: bool = True
    sql: str

    def __init__(self, sql: str = None):
        self.logger = logging.getLogger(__name__)
        self.sql = sql

        if sql is not None:
            self.is_empty = False
            self.__is_it_dangerous()

    def __is_it_dangerous(self) -> None:
        self.is_dangerous = False
        items = self.sql.split(' ')
        for i in items:
            if i.upper().strip() in UNSAFE_SQL_STATEMENT_LIST:
                self.is_dangerous = True
                self.logger.warning("Unsafe SQL statement detected. "
                                    "This application will not be able to "
                                    "execute unless an override is set.")
                break
