import logging
import pandas as pd


class AggregatedDataObject:
    aggregate_data = None

    """
    This class represents an aggregated data object.
    Takes a list of pandas dataframes and flattens them
    into a single dataframe.
    """
    def __init__(self, dataframes: list):
        self.__dataframe_list = dataframes
        self.__flatten_data()

    def __flatten_data(self) -> pd.DataFrame:
        """
        This method flattens multiple dataframes
        into a single dataframe.
        """
        self.aggregate_data = pd.concat(self.dataframe_list)


class Exporter:
    """
    This class is used to export the dataset to known
    readable formats like:
    - csv
    - xslx
    - json
    - console
    """
    def __init__(self) -> None:
        self.logger = logging.getLogger(__name__)

    def export_csv(self,
                   dataset: pd.DataFrame,
                   output_path: str) -> None:
        """
        This function exports data into a CSV format.
        """
        raise NotImplementedError

    def export_xslx(self,
                    dataset: pd.DataFrame,
                    output_path: str) -> None:
        """
        This function exports data into an Microsoft XLSX format.
        """
        raise NotImplementedError


class XSLXExport():

    def __init__(self):
        raise NotImplementedError


class CSVExport():

    def __init__(self):
        raise NotImplementedError
