from unittest import TestCase
from unittest.mock import patch
from databasemultiquery.sql import Sql

test_statement = """
SELECT *
    FROM test_table;
"""

test_blocked_statement_1 = """
DROP TABLE test_table;
"""

test_blocked_statement_2 = """
INSERT INTO test_table (test_column) VALUES (test_value);
"""

test_blocked_statement_3 = """
DELETE FROM test_table;
"""

test_blocked_statement_4 = """
UPDATE test_table SET test_column = test_value;
"""

test_blocked_statement_5 = """
CREATE TABLE test_table (test_column);
"""

class TestSql(TestCase):

    def test_instantiation(self):
        statement = Sql(test_statement)
        self.assertIsInstance(statement, Sql)
        self.assertEqual(statement.sql, test_statement)

    def test_is_empty(self):
        statement = Sql()
        self.assertTrue(statement.is_empty)
    
    def test_is_dangerous(self):
        statement = Sql(test_statement)
        self.assertFalse(statement.is_dangerous)

    def test_is_dangerous_with_blocked_statement(self):
        statement_1 = Sql(test_blocked_statement_1)
        self.assertTrue(statement_1.is_dangerous)
        statement_2 = Sql(test_blocked_statement_2)
        self.assertTrue(statement_2.is_dangerous)        
        statement_3 = Sql(test_blocked_statement_3)
        self.assertTrue(statement_3.is_dangerous)
        statement_4 = Sql(test_blocked_statement_4)
        self.assertTrue(statement_4.is_dangerous)
        statement_5 = Sql(test_blocked_statement_5)
        self.assertTrue(statement_4.is_dangerous)              
