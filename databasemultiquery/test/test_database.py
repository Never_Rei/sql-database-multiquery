from unittest import TestCase
from unittest.mock import patch, mock_open
from databasemultiquery.database import DatabaseConfig, DatabaseConfigLoader, DatabaseClient

import yaml

TEST_CONFIG_ATTRIBUTES_1={
    'user': 'test_user_1',
    'password': 'test_password_1',
    'host': 'test_host_1',
    'port': 1234,
    'database': 'test_database_1',
    'driver' : 'test_driver_1'
}

TEST_CONFIG_ATTRIBUTES_2={
    'user': 'test_user_2',
    'password': 'test_password_2',
    'host': 'test_host_2',
    'port': 1234,
    'database': 'test_database_2',
    'driver' : 'test_driver_2'
}

TEST_CONFIG_BLOCKS={
    "database": [
        TEST_CONFIG_ATTRIBUTES_1,
        TEST_CONFIG_ATTRIBUTES_2
    ]
}


class TestDatabaseConfig(TestCase):

    def test_instantiation(self):
        conf = DatabaseConfig()
        self.assertIsInstance(conf, DatabaseConfig)
        with self.assertRaises(AttributeError):
            conf.connection_string

    def test_attributes(self):
        conf = DatabaseConfig()
        self.assertEqual(conf.user, None)
        self.assertEqual(conf.password, None)
        self.assertEqual(conf.host, None)
        self.assertEqual(conf.port, 0)
        self.assertEqual(conf.database, None)
        self.assertEqual(conf.driver, None)

    def test_set_attributes_from_object(self):
        conf = DatabaseConfig()
        conf.load_config_from_object(TEST_CONFIG_ATTRIBUTES_1)
        self.assertEqual(conf.user, TEST_CONFIG_ATTRIBUTES_1['user'])
        self.assertEqual(conf.password, TEST_CONFIG_ATTRIBUTES_1['password'])
        self.assertEqual(conf.host, TEST_CONFIG_ATTRIBUTES_1['host'])
        self.assertEqual(conf.port, TEST_CONFIG_ATTRIBUTES_1['port'])
        self.assertEqual(conf.database, TEST_CONFIG_ATTRIBUTES_1['database'])
        self.assertEqual(conf.driver, TEST_CONFIG_ATTRIBUTES_1['driver'])
    
    def test_set_attributes_from_object_invalid_parameter(self):
        conf = DatabaseConfig()
        with self.assertRaises(NameError):
            conf.load_config_from_object({'invalid_parameter': 'test_value'})


class TestDatabaseConfigLoader(TestCase):

    @patch("os.path.exists", return_value=True)
    @patch("builtins.open", mock_open(read_data=yaml.dump(TEST_CONFIG_BLOCKS)))
    def test_instantiation(self, mock_exists):
        db_config = DatabaseConfigLoader('test_config.yml')
        self.assertIsInstance(db_config, DatabaseConfigLoader)


class TestDatabaseClient(TestCase):

    @patch.multiple(DatabaseClient, __abstractmethods__=set())
    def test_instantiation(self):
        db_client = DatabaseClient()
        self.assertIsInstance(db_client, DatabaseClient)